package br.org.paulistano.trainnigEleicaoTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainnigEleicaoTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainnigEleicaoTestApplication.class, args);
	}
}
