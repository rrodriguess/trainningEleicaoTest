package br.org.paulistano.trainnigEleicaoTest.domain.entities;

import br.org.paulistano.trainnigEleicaoTest.domain.enums.NonVoteType;
import br.org.paulistano.trainnigEleicaoTest.domain.utils.Vote;

import javax.persistence.*;

@Entity(name = "VotoNuloBranco")
public class VoteNullWhite extends Vote {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    private NonVoteType type;

    public VoteNullWhite() {

    }

    public VoteNullWhite(Section section, Urn urn, NonVoteType type) {
        super(section, urn);
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NonVoteType getType() {
        return type;
    }

    public void setType(NonVoteType type) {
        this.type = type;
    }

}
