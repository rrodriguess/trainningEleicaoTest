package br.org.paulistano.trainnigEleicaoTest.domain.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity(name = "Secao")
public class Section implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Boolean contested = Boolean.FALSE;

    @NotNull
    @Column(name = "habilitado_pos")
    private Boolean availablePos = Boolean.TRUE;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getContested() {
        return contested;
    }

    public void setContested(Boolean contested) {
        this.contested = contested;
    }

    public Boolean getAvailablePos() {
        return availablePos;
    }

    public void setAvailablePos(Boolean availablePos) {
        this.availablePos = availablePos;
    }
}
