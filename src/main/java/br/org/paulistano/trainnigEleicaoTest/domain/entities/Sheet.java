package br.org.paulistano.trainnigEleicaoTest.domain.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Chapa")
public class Sheet {

    @Id
    private Long id;

    @NotEmpty
    @Column(name = "nome", length = 20)
    private String name;

    @NotNull
    @Column(name = "ordemVisualizacao", length = 1)
    private Integer displayOrder;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "sheet")
    private List<Candidate> candidates = new ArrayList<>();


}
