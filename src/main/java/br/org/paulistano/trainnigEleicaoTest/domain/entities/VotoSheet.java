package br.org.paulistano.trainnigEleicaoTest.domain.entities;

import javax.persistence.*;

@Entity(name = "VotoChapa")
@Table(schema = "pollingServer")
public class VotoSheet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Boolean contested;

    private Long sheetId;

    private Long sectionId;

    private Long urnId;

    public Long getId() {
        return id;
    }

    public Boolean getContested() {
        return contested;
    }

    public void setContested(Boolean contested) {
        this.contested = contested;
    }

    public Long getSheetId() {
        return sheetId;
    }

    public void setSheetId(Long sheetId) {
        this.sheetId = sheetId;
    }

    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    public Long getUrnId() {
        return urnId;
    }

    public void setUrnId(Long urnId) {
        this.urnId = urnId;
    }

    @Override
    public String toString() {
        return "VotoSheet{" +
                "id=" + id +
                ", contested=" + contested +
                ", sheetId=" + sheetId +
                ", sectionId=" + sectionId +
                ", urnId=" + urnId +
                '}';
    }
}
