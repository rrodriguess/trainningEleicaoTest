package br.org.paulistano.trainnigEleicaoTest.domain.enums;

/**
 * Created by renato on 21/09/17.
 */
public enum Roles {

    ROLE_ADMIN("Administrador"), ROLE_OPERATOR("Operador"), ROLE_URN("Urna"), ROLE_POS("Pos");

    private String desc;

    Roles(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
