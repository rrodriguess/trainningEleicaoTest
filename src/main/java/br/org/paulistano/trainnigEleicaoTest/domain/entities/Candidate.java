package br.org.paulistano.trainnigEleicaoTest.domain.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity (name = "Candidato")
public class Candidate {

    @Id
    private Long id;

    @NotEmpty
    @Column(name = "nome")
    private String name;

    @NotNull
    @Column(name = "data_nascimento", nullable = false)
    private LocalDateTime dateBirth;

    @NotNull
    @Column(name = "data_efetividade", nullable = false)
    private LocalDateTime dateEffectiveness;

    @ManyToOne
    @JoinColumn(name = "candidate_category_id")
    private CandidateCategory candidateCategory;

    @ManyToOne
    @JoinColumn(name = "chapa_id")
    private Sheet sheet;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(LocalDateTime dateBirth) {
        this.dateBirth = dateBirth;
    }

    public LocalDateTime getDateEffectiveness() {
        return dateEffectiveness;
    }

    public void setDateEffectiveness(LocalDateTime dateEffectiveness) {
        this.dateEffectiveness = dateEffectiveness;
    }

    public CandidateCategory getCandidateCategory() {
        return candidateCategory;
    }

    public void setCandidateCategory(CandidateCategory candidateCategory) {
        this.candidateCategory = candidateCategory;
    }

    public Sheet getSheet() {
        return sheet;
    }

    public void setSheet(Sheet sheet) {
        this.sheet = sheet;
    }
}
