package br.org.paulistano.trainnigEleicaoTest.domain.enums;

/**
 * Created by renato on 21/09/17.
 */
public enum UrnStatus {

    IN_USE("Em uso"), AVAILABLE("Livre"), BACKUP("Reserva"), BLOCKED("Urna Bloqueada");

    private String message;

    private UrnStatus(String message) {
        this.message = message;
    }

    private String getMessage() {
        return message;
    }
}
