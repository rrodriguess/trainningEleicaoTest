package br.org.paulistano.trainnigEleicaoTest.domain.utils;

import br.org.paulistano.trainnigEleicaoTest.domain.entities.Section;
import br.org.paulistano.trainnigEleicaoTest.domain.entities.Urn;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public abstract class Vote {

    @NotNull
    @OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Section section;

    @NotNull
    @OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Urn urn;

    @NotNull
    private Boolean contested = Boolean.FALSE;

    public Vote() {
    }

    public Vote(Section section, Urn urn) {
        this.section = section;
        this.urn = urn;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Urn getUrn() {
        return urn;
    }

    public void setUrn(Urn urn) {
        this.urn = urn;
    }

    public Boolean getContested() {
        return contested;
    }

    public void setContested(Boolean contested) {
        this.contested = contested;
    }
}
