package br.org.paulistano.trainnigEleicaoTest.domain.entities;

import br.org.paulistano.trainnigEleicaoTest.domain.utils.Vote;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "VotoCandidato")
public class VoteCandidate extends Vote {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Candidate candidate;

    public VoteCandidate() {

    }

    public VoteCandidate(Candidate candidate, Section section, Urn urn) {
        super(section, urn);
        this.candidate = candidate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((candidate == null) ? 0 : candidate.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VoteCandidate other = (VoteCandidate) obj;
        if (candidate == null) {
            if (other.candidate != null)
                return false;
        } else if (!candidate.equals(other.candidate))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }


}
