package br.org.paulistano.trainnigEleicaoTest.domain.entities;

import br.org.paulistano.trainnigEleicaoTest.domain.enums.MachineType;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "MachineConfig")
public class MachineConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private MachineType type;

    @NotEmpty
    private String ip;

    @NotNull
    private Boolean blocked = Boolean.FALSE;

    @Transient
    private String genericInfo;

    public Long getId() {
        return id;
    }

    public MachineType getType() {
        return type;
    }

    public void setType(MachineType type) {
        this.type = type;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public String getGenericInfo() {
        return genericInfo;
    }

    public void setGenericInfo(String genericInfo) {
        this.genericInfo = genericInfo;
    }
}
