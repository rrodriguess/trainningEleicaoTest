package br.org.paulistano.trainnigEleicaoTest.domain.enums;

/**
 * Created by renato on 21/09/17.
 */
public enum NonVoteType {

    WHITE("WHITE"), NULL("NULL");

    private String type;

    private NonVoteType(String type) {
        this.setType(type);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
