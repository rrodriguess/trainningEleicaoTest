package br.org.paulistano.trainnigEleicaoTest.domain.entities;

import jdk.nashorn.internal.ir.annotations.Immutable;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Immutable
@Entity(name = "Categoria")
public class CandidateCategory {

    @Id
    private Long id;

    @NotEmpty
    @Column(name = "nome")
    private String name;

    @NotNull
    @Column(name = "vigencia_nove_anos")
    private Long vigenciaNoveAnos;

    @NotNull
    @Column(name = "vigencia_seis_anos")
    private Long vigenciaSeisAnos;

    @NotNull
    @Column(name = "vigencia_tres_anos")
    private Long vigenciaTresAnos;

    public CandidateCategory() {
    }

    public CandidateCategory(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getVigenciaNoveAnos() {
        return vigenciaNoveAnos;
    }

    public void setVigenciaNoveAnos(Long vigenciaNoveAnos) {
        this.vigenciaNoveAnos = vigenciaNoveAnos;
    }

    public Long getVigenciaSeisAnos() {
        return vigenciaSeisAnos;
    }

    public void setVigenciaSeisAnos(Long vigenciaSeisAnos) {
        this.vigenciaSeisAnos = vigenciaSeisAnos;
    }

    public Long getVigenciaTresAnos() {
        return vigenciaTresAnos;
    }

    public void setVigenciaTresAnos(Long vigenciaTresAnos) {
        this.vigenciaTresAnos = vigenciaTresAnos;
    }
}
