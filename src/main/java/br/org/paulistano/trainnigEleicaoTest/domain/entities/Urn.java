package br.org.paulistano.trainnigEleicaoTest.domain.entities;

import br.org.paulistano.trainnigEleicaoTest.domain.enums.UrnStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "Urna")
public class Urn {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ordem")
    private Long order;

    @NotNull
    private Boolean contested = Boolean.FALSE;

    @Enumerated(EnumType.STRING)
    private UrnStatus status = UrnStatus.AVAILABLE;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private MachineConfig machineConfig;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public Boolean getContested() {
        return contested;
    }

    public void setContested(Boolean contested) {
        this.contested = contested;
    }
}
