package br.org.paulistano.trainnigEleicaoTest.domain.enums;

/**
 * Created by renato on 21/09/17.
 */
public enum MachineType {

    VOTING("Urna"), ADMIN("Administrativo"), SECTION_11("Seção 11"), POS(
            "Ponto de Venda", Boolean.FALSE);

    private String message;

    private Boolean show = Boolean.TRUE;

    private MachineType(String message, Boolean show) {
        this.message = message;
        this.show = show;
    }

    private MachineType(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Boolean getShow() {
        return show;
    }

    public static MachineType getMachineType(String message) {
        for (MachineType machineType : values()) {
            if (machineType.getMessage().equals(message)) {
                return machineType;
            }
        }

        return null;
    }
}
