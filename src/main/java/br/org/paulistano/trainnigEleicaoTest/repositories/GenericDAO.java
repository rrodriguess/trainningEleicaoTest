package br.org.paulistano.trainnigEleicaoTest.repositories;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

/**
 * This follows the standard generic DAO implementation seen in thousands of
 * frameworks.
 * <p>
 * This implementation was borrowed from (with added key strong typing):
 * http://insidecoding.wordpress.com/2011/
 * 09/07/the-generic-dao-pattern-in-java-with-spring-3-and-jpa-2-0/
 * <p>
 *
 * @param <T>
 *            The type
 * @param <K>
 *            The id type
 */
public interface GenericDAO<T extends Serializable, K extends Serializable> {

    Session session();

    /**
     * Persists a new Instance
     *
     * @param t
     *            The instance to be persisted
     * @return The managed instance (with ID)
     */
    K create(T t);

    /** Permanently deletes a record. */
    void delete(T target);

    /**
     * Retrieves a managed entity by its id
     *
     * @param id
     *            The id of the record that will be fetched
     * @return The managed instance (with ID)
     */
    T find(K id);

    /**
     * Updates an entity (that has an ID already)
     *
     * @param t
     *            The entity that will be updated
     * @return The managed instance (with ID)
     */
    T update(T t);

    /**
     * Retrieves a managed all entity
     *
     * @return The managed instance (with FIELD)
     */
    List<T> findAll();

    Long count();

    void deleteAll(String myTable);
}