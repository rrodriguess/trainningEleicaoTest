package br.org.paulistano.trainnigEleicaoTest.repositories.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.List;

import br.org.paulistano.trainnigEleicaoTest.domain.utils.Auditable;
import br.org.paulistano.trainnigEleicaoTest.repositories.GenericDAO;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


@SuppressWarnings("unchecked")
public abstract class GenericDAOImpl<T extends Serializable, K extends Serializable>
        implements GenericDAO<T, K> {

    @Autowired
    protected SessionFactory sessionFactory;

    private final Class<T> type;

    /**
     * This constructor calls the method getClassOfTypeParameter() to
     * instantiate the class of the given parameterized type. It is up to any
     * subclass to decide if it is needed to override the default
     * implementation.
     */
    public GenericDAOImpl() {
        type = getClassOfTypeParameter();
    }

    public Session session() {
        return sessionFactory.getCurrentSession();
    }

    protected Criteria criteria() {
        return session().createCriteria(type);
    }

    /**
     * Uses the GenericSuperclass approach to get the class object of the class
     * first Type parameter.
     *
     * @return the class of type T
     */
    protected Class<T> getClassOfTypeParameter() {
        final Type t = getClass().getGenericSuperclass();
        final ParameterizedType pt = (ParameterizedType) t;
        return (Class<T>) pt.getActualTypeArguments()[0];
    }

    @Override
    @Transactional
    public K create(final T t) {
        return (K) session().save(t);
    }

    @Override
    @Transactional
    public void delete(final T target) {
        session().delete(target);
    }

    /**
     * @param @String myTable Nome exato da tabela na base de dados.
     */
    @Override
    @Transactional
    public void deleteAll(String myTable) {
        String hql = String.format("DELETE FROM %s", myTable);
        Query query = session().createQuery(hql);
        query.executeUpdate();
    }

    @Override
    @Transactional(readOnly = true)
    public T find(final K id) {
        return (T) session().get(type, id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<T> findAll() {
        return session().createCriteria(type).list();
    }

    @Override
    @Transactional
    public T update(final T t) {

        if (t instanceof Auditable)
            ((Auditable) t).setUpdatedDate(LocalDateTime.now());

        session().update(t);
        return t;
    }

    @Override
    @Transactional(readOnly = true)
    public Long count() {
        return (Long) criteria().setProjection(Projections.rowCount())
                .uniqueResult();
    }

}